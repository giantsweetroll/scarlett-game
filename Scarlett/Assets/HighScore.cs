using UnityEngine.UI;
using UnityEngine;

public class HighScore : MonoBehaviour
{
    // Fields
    public Text scoreText;

    void Update()
    {
        scoreText.text = "High Score: " + GameManager.highScore.ToString("0");
    }
}
