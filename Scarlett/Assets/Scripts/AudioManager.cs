using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public Slider masterVolumeSlider;

    public void Start()
    {
        masterVolumeSlider.value = AudioListener.volume * 100f;
    }

    public void updateMasterVolume(float newValue)
    {
        AudioListener.volume = newValue / 100f;
    }
}
