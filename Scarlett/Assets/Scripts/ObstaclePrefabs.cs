using System.Collections.Generic;
using UnityEngine;

public class ObstaclePrefabs
{
    public List<float> generateSimpleGroundObstacles(int maxObstaclesPerRow, float spawnChance)
    {
        List<float> randValues = new List<float>();

        while (true)
        {
            for (int i = 0; i < maxObstaclesPerRow; i++)
            {
                randValues.Add(Random.value);
            }

            if (!this.hasGap(randValues, spawnChance))
            {
                randValues.Clear();
            }
            else
            {
                break;
            }
        }

        return randValues;
    }

    public List<List<float>> generateRandomStackingObstacle(int maxObstaclesPerRow, float spawnChance, int maxHeight, bool uniform)
    {
        List<List<float>> ls = new List<List<float>>();

        ls.Add(this.generateSimpleGroundObstacles(maxObstaclesPerRow, spawnChance));

        if (uniform)
        {
            for (int i = 0; i < maxHeight; i++)
            {
                List<float> sub = new List<float>();
                for (int a = 0; a < ls[0].Count; a++)
                {
                    // Only add obstacle if there is one below it
                    if (this.allowSpawn(ls[0][a], spawnChance)) sub.Add(0.0f);       // 0.0f for absolute certainty it will spawn
                    else sub.Add(1.1f);     // 1.1f for absolute certainty it will NOT spawn
                }
                ls.Add(sub);
            }
        }
        else
        {
            for (int i = 0; i < maxHeight; i++)
            {
                List<float> sub = new List<float>();
                for (int a = 0; a < ls[i].Count; a++)
                {
                    // Only add obstacle if there is one below it
                    if (this.allowSpawn(ls[i][a], spawnChance))
                    {
                        sub.Add(Random.value);
                    }
                    else sub.Add(1.1f);     // 1.1f for absolute certainty it will NOT spawn
                }
                ls.Add(sub);
            }
        }

        return ls;
    }

    public List<List<float>> generateFallingObstacles(int maxObstaclesPerRow, float spawnChance, int fallHeight, int itemIndex = -1)
    {
        List<List<float>> ls = new List<List<float>>();

        for (int i=0; i<fallHeight; i++)
        {
            List<float> sub = new List<float>();
            // Create empty dimensions (leave the last for spawner)
            for (int a = 0; a < maxObstaclesPerRow - 1; a++)
            {
                sub.Add(1.1f);      // Do not permit any spawn
            }
            ls.Add(sub);
        }

        List<float> spawnerList = new List<float>();
        // Fill last item with spawner
        while (true)
        { 
            for (int i = 0; i < maxObstaclesPerRow; i++)
            {
                spawnerList.Add(Random.value);
            }

            if (!this.hasGap(spawnerList, spawnChance)) spawnerList.Clear();
            else break;
        }
        ls.Add(spawnerList);

        return ls;
    }

    private bool allowSpawn(float val, float spawnChance)      // Check if the value allows an obstacle to be spawned there
    {
        return val <= spawnChance;
    }

    private bool hasGap(List<float> obstacles, float spawnChance, int minGap = 2)
    {
        // Check validity (make sure there is at least X adjacent empty space for the player to go through)
        int counter = 0;
        foreach (float val in obstacles)
        {
            if (val > spawnChance)
            {
                counter++;
                if (counter == minGap)
                {
                    return true;
                }
            }
            else
            {
                counter = 0;
            }
        }

        return false;
    }
}
