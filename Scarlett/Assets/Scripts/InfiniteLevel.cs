using System.Collections.Generic;
using System;

public class InfiniteLevel : SpawnObstacle
{
    private ObstaclePrefabs obstacles = new ObstaclePrefabs();

    public override List<List<float>> generateObstacles()
    {
        List<List<float>> ls = new List<List<float>>();

        this.spawneeIndex = 0;

        if (this.playerTransform.position.z < 1000)
        {
            // Simple ground obstacle
            ls.Add(obstacles.generateSimpleGroundObstacles(this.maxObstaclesPerRow, this.spawnChance));
       //     this.spawneeIndex = this.spawnees.Count - 1;      // Mark to spawn simple multi spawner
       //     ls.AddRange(obstacles.generateFallingObstacles(this.maxObstaclesPerRow, this.spawnChance, 8));
        }
        else if (this.playerTransform.position.z < 1500)
        {
            // Obstacle with varying height
            ls.AddRange(obstacles.generateRandomStackingObstacle(this.maxObstaclesPerRow, this.spawnChance, 5, false));
        }
        else if (this.playerTransform.position.z < 2000)
        {
            // Obstacle with full height
            ls.AddRange(obstacles.generateRandomStackingObstacle(this.maxObstaclesPerRow, this.spawnChance, 5, true));
        }
        else
        {
            // Mix of all, selected by random
            Random random = new Random();
            int val = random.Next(3);
            if (val == 0) ls.Add(obstacles.generateSimpleGroundObstacles(this.maxObstaclesPerRow, this.spawnChance));
            else if (val == 1) ls.AddRange(obstacles.generateRandomStackingObstacle(this.maxObstaclesPerRow, this.spawnChance, 5, false));
            else if (val == 2) ls.AddRange(obstacles.generateRandomStackingObstacle(this.maxObstaclesPerRow, this.spawnChance, 5, true));
            else ls.AddRange(obstacles.generateRandomStackingObstacle(this.maxObstaclesPerRow, this.spawnChance, 5, false));
        }/*
        else if (this.playerTransform.position.z < 3000)
        {
            this.spawneeIndex = this.spawnees.Count-1;      // Mark to spawn simple multi spawner
            ls.AddRange(obstacles.generateFallingObstacles(this.maxObstaclesPerRow, this.spawnChance, 8));
        }   */

        return ls;
    }

    protected override void OnBeforeObstaclesSpawned()
    {
        if (this.playerTransform.position.z <= 500 || this.spawneeIndex == this.spawnees.Count-1)
        {
            this.spawnChance = 0.2f;
        }
        else
        {
            this.spawnChance = 0.4f;
        }
    }

    protected override void OnObstaclesSpawned()
    {

    }
}
