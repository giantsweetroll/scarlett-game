using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    //This is a reference to the Rigidbody component called "rb"
    public Rigidbody rb;

    public float forwardForce = 2000f;
    public float sidewaysForce = 500f;

    // Update is called once per frame
    void FixedUpdate()
    {
        // Add a forward force
        rb.AddForce(0, 0, this.forwardForce * Time.deltaTime);

        if (Input.GetKey("d"))
        {
            // Add force to the right
            rb.AddForce(this.sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (Input.GetKey("a"))
        {
            // Add force to the left
            rb.AddForce(-this.sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }

        // Update player position value
        if (!GameManager.gameHasEnded && this.transform.position.y + this.transform.localScale.y / 2 >= 0)
        {
            GameManager.currentDistance = this.transform.position.z;
            if (GameManager.currentDistance > GameManager.highScore)
                GameManager.highScore = GameManager.currentDistance;
        }
    }
}
