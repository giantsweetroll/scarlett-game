using UnityEngine;

public class GroundSpawner : MonoBehaviour
{
    // Fields
    public GameObject spawnee;
    public Transform groundTransform, playerTransform;
    public bool stopSpawning = false;
    public float spawnChance = 0.2f;        // Value between 0.0 and 1.0 inclusive
    public float initialDistanceFromPlayer = 200f;
    public float distanceBetweenSpawns = 100f;
    public int maxSpawnIter = -1;         // Max number of rows to spawn, -1 means infinite

    private int curSpawnIter = 0;        // keeps track of current iteration
    private int maxObstaclesPerRow = 0; // Max possible amount of obstacles spawned per row
    private float[] curObstacleRow;
    private int i = 0;

    // Start is called before the first frame update
    void Start()
    {
        // Change spawner location
        Vector3 initialPos = this.transform.position;
        initialPos.x = this.getGroundLeftBoundary();
        initialPos.z = this.playerTransform.position.z + this.initialDistanceFromPlayer;
        this.transform.position = initialPos;

        // Calculate max possible amount of obstacles spawned per row
        Vector3 curPos = this.transform.position;
        while (curPos.x <= this.getGroundRightBoundary())
        {
            maxObstaclesPerRow++;
            curPos.x += this.transform.localScale.x;
        }
        this.curObstacleRow = this.generateObstacles();
    }

    void Update()
    {
        if (this.transform.position.z - this.playerTransform.position.z <= this.initialDistanceFromPlayer)
        {
            this.stopSpawning = false;
        }

        if (!stopSpawning)
        {
            // Spawn obstacle by chance
            if (this.curObstacleRow[i] <= spawnChance)
            {
                Invoke("SpawnObject", 0);
            }

            // Move spawner to the right
            Vector3 newPos = this.transform.position;
            newPos.x += this.transform.localScale.x;       // Move it to adjacent position
            // Check if the new pos is out of bounds, if it is, set it back to the left most side of the ground and increment the z value.
            if (newPos.x - this.transform.localScale.x / 2 > this.getGroundRightBoundary())
            {
                newPos.x = this.getGroundLeftBoundary();
                newPos.z += distanceBetweenSpawns;
                i = -1;
                this.curSpawnIter++;

                this.curObstacleRow = this.generateObstacles();

                if (curSpawnIter == maxSpawnIter || newPos.z - this.playerTransform.position.z > this.initialDistanceFromPlayer)
                {
                    this.stopSpawning = true;
                }
            }
            this.transform.position = newPos;   // Update spawner position
            i++;
        }
    }

    public void SpawnObject()
    {
        Instantiate(spawnee, transform.position, transform.rotation);
    }

    private float getGroundLeftBoundary()
    {
        return this.groundTransform.position.x - this.groundTransform.localScale.x / 2;
    }

    private float getGroundRightBoundary()
    {
        return this.groundTransform.position.x + this.groundTransform.localScale.x / 2;
    }
    private float[] generateObstacles()
    {
        float[] randValues = new float[this.maxObstaclesPerRow];

        while (true)
        {
            for (int i = 0; i < this.maxObstaclesPerRow; i++)
            {
                randValues[i] = Random.value;
            }

            // Check validity (make sure there is at least 2 adjacent empty space for the player to go through)
            bool hasGap = false;
            int counter = 0;
            foreach (float val in randValues)
            {
                if (val > this.spawnChance)
                {
                    counter++;
                    if (counter == 2)
                    {
                        hasGap = true;
                        break;
                    }
                }
            }

            if (hasGap) break;
        }

        return randValues;
    }
}
