using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2D : MonoBehaviour
{
    // Fields
    public GameObject spawnee;
    public Transform groundTransform, playerTransform;
    public bool stopSpawning = false;
    public float spawnChance = 0.2f;        // Value between 0.0 and 1.0 inclusive
    public float initialDistanceFromPlayer = 200f;
    public float distanceBetweenSpawns = 100f;
    public int maxSpawnIter = -1;         // Max number of rows to spawn, -1 means infinite

    private int curSpawnIter = 0;        // keeps track of current iteration
    private int maxObstaclesPerRow = 0; // Max possible amount of obstacles spawned per row
    private List<List<float>> obstacles = new List<List<float>>();

    // Start is called before the first frame update
    void Start()
    {
        // Check for invalid input
        if (spawnChance > 1.0f) spawnChance = 1.0f;
        else if (spawnChance < 0.0f) spawnChance = 0.0f;

        // Change spawner location
        Vector3 initialPos = this.transform.position;
        initialPos.x = this.getGroundLeftBoundary();
        initialPos.y = 1;
        initialPos.z = this.playerTransform.position.z + this.initialDistanceFromPlayer;
        this.transform.position = initialPos;

        // Calculate max possible amount of obstacles spawned per row
        Vector3 curPos = this.transform.position;
        while (curPos.x <= this.getGroundRightBoundary())
        {
            maxObstaclesPerRow++;
            curPos.x += this.transform.localScale.x;
        }
    }

    void Update()
    {
        if (this.transform.position.z - this.playerTransform.position.z <= this.initialDistanceFromPlayer)
        {
            this.stopSpawning = false;
        }

        if (!stopSpawning)
        {
            this.obstacles = this.generateRandomStackingObstalce(5, true);
            Vector3 newPos = this.transform.position;
            for (int row = 0; row < this.obstacles.Count; row++)
            {
                for (int col = 0; col < this.obstacles[row].Count; col++)
                {
                    // Spawn obstacle by chance
                    if (this.allowSpawn(this.obstacles[row][col]))
                    {
                        //Invoke("SpawnObject", 0);
                        Instantiate(this.spawnee, newPos, transform.rotation);
                    }

                    // Move spawner to the right
                    newPos.x += this.transform.localScale.x;       // Move it to adjacent position
                }

                newPos.x = this.getGroundLeftBoundary();        // Reset spawner x to the left side of the ground
                newPos.y += this.transform.localScale.y;        // Move spawner y up
            }
            this.curSpawnIter++;
            newPos.y = 1;
            newPos.z += distanceBetweenSpawns;
            this.transform.position = newPos;   // Update spawner position

            if (curSpawnIter == maxSpawnIter || newPos.z - this.playerTransform.position.z > this.initialDistanceFromPlayer)
            {
                this.stopSpawning = true;
            }
        }
    }

    public void SpawnObject()
    {
        Instantiate(spawnee, transform.position, transform.rotation);
    }

    private bool allowSpawn(float val)      // Check if the value allows an obstacle to be spawned there
    {
        return val <= spawnChance;
    }

    private float getGroundLeftBoundary()
    {
        return this.groundTransform.position.x - this.groundTransform.localScale.x / 2 + this.transform.localScale.x/2;
    }

    private float getGroundRightBoundary()
    {
        return this.groundTransform.position.x + this.groundTransform.localScale.x / 2 - this.transform.localScale.x/2;
    }
    private List<List<float>> generateObstacles()
    {
        List<List<float>> ls = new List<List<float>>();

        ls.Add(this.generateSimpleGroundObstacles());

        return ls;
    }

    private List<float> generateSimpleGroundObstacles()
    {
        List<float> randValues = new List<float>();

        while (true)
        {
            for (int i = 0; i < this.maxObstaclesPerRow; i++)
            {
                randValues.Add(Random.value);
            }

            // Check validity (make sure there is at least 2 adjacent empty space for the player to go through)
            bool hasGap = false;
            int counter = 0;
            foreach (float val in randValues)
            {
                if (val > this.spawnChance)
                {
                    counter++;
                    if (counter == 2)
                    {
                        hasGap = true;
                        break;
                    }
                }
            }

            if (hasGap) break;
        }

        return randValues;
    }

    private List<List<float>> generateRandomStackingObstalce(int maxHeight, bool uniform)
    {
        List<List<float>> ls = new List<List<float>>();

        ls.Add(this.generateSimpleGroundObstacles());

        if (uniform)
        {
            for (int i=0; i<maxHeight; i++)
            {
                List<float> sub = new List<float>();
                for (int a=0; a<ls[0].Count; a++)
                {
                    if (this.allowSpawn(ls[0][a])) sub.Add(0.0f);       // 0.0f for absolute certainty it will spawn
                    else sub.Add(1.1f);     // 1.1f for absolute certainty it will NOT spawn
                }
                ls.Add(sub);
            }
        }

        return ls;
    }
}
