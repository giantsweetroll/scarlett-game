using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;

    private AudioSource collisionAudio;
    private int collisionCounter = 0;

    public void Awake()
    {
        this.collisionAudio = this.gameObject.GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionCounter == 0)
        {
            this.collisionCounter++;
            return;
        }

        AudioSource.PlayClipAtPoint(this.collisionAudio.clip, this.transform.position); // Play collision audio

        if (collisionInfo.collider.tag == "Obstacle")
        {
            
            this.movement.enabled = false;      // Disable player controls
            FindObjectOfType<GameManager>().EndGame();      // End game and reset
        }
    }
}
