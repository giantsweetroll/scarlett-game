using UnityEngine;

public class FollowObjectZ : MonoBehaviour
{
    public Transform obj;
    public float zOffset;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = this.transform.position;
        newPos.z = obj.position.z + zOffset;
        transform.position = newPos;
    }
}
