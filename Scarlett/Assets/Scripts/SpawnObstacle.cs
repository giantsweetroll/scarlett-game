using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnObstacle : MonoBehaviour
{
    // Fields
    public List<GameObject> spawnees;
    public Transform groundTransform, playerTransform;
    public bool stopSpawning = false;
    public float spawnChance = 0.2f;        // Value between 0.0 and 1.0 inclusive
    public float initialDistanceFromPlayer = 200f;
    public float distanceBetweenSpawns = 100f;
    public int maxSpawnIter = -1;         // Max number of rows to spawn, -1 means infinite

    protected int spawneeIndex = 0;
    protected int maxObstaclesPerRow = 0; // Max possible amount of obstacles spawned per row

    private int curSpawnIter = 0;        // keeps track of current iteration
    private List<List<float>> obstacles = new List<List<float>>();

    // Start is called before the first frame update
    void Start()
    {
        // Check for invalid input
        if (spawnChance > 1.0f) spawnChance = 1.0f;
        else if (spawnChance < 0.0f) spawnChance = 0.0f;

        // Change spawner location
        Vector3 initialPos = this.transform.position;
        initialPos.x = this.getGroundLeftBoundary();
        initialPos.y = 1;
        initialPos.z = this.playerTransform.position.z + this.initialDistanceFromPlayer;
        this.transform.position = initialPos;

        // Calculate max possible amount of obstacles spawned per row
        Vector3 curPos = this.transform.position;
        while (curPos.x <= this.getGroundRightBoundary())
        {
            maxObstaclesPerRow++;
            curPos.x += this.transform.localScale.x;
        }
    }

    public void Update()
    {
        if (this.transform.position.z - this.playerTransform.position.z <= this.initialDistanceFromPlayer)
        {
            this.stopSpawning = false;
        }

        if (!stopSpawning)
        {
            this.OnBeforeObstaclesSpawned();
            this.obstacles = this.generateObstacles();
            Vector3 newPos = this.transform.position;
            for (int row = 0; row < this.obstacles.Count; row++)
            {
                for (int col = 0; col < this.obstacles[row].Count; col++)
                {
                    // Spawn obstacle by chance
                    if (this.allowSpawn(this.obstacles[row][col]))
                    {
                        if (this.spawneeIndex != this.spawnees.Count-1)
                        {
                            GameObject obstacle = this.spawnees[this.spawneeIndex];
                            obstacle.GetComponent<ObstacleCollision>().playerTransform = this.playerTransform;

                            Instantiate(obstacle, newPos, transform.rotation);
                        }
                        else
                        {
                            GameObject spawner = this.spawnees[this.spawneeIndex];

                            // Add destroy after time script
                            spawner.AddComponent<DestroyAfterTime>();

                            // Remove the spawner mesh from rendering
                            MeshRenderer meshRender = spawner.GetComponent<MeshRenderer>();
                            meshRender.enabled = false;

                            // Manage timed spawn settings
                            TimedSpawnMulti tsm = spawner.GetComponent<TimedSpawnMulti>();
                            tsm.spawnDelay = Random.Range(0.5f, 1);
                            tsm.randomizeSpawnOffset = true;

                            Instantiate(spawner, newPos, transform.rotation);
                        }
                    }

                    // Move spawner to the right
                    newPos.x += this.transform.localScale.x;       // Move it to adjacent position
                }

                newPos.x = this.getGroundLeftBoundary();        // Reset spawner x to the left side of the ground
                newPos.y += this.transform.localScale.y;        // Move spawner y up
            }
            this.curSpawnIter++;
            newPos.y = 1;
            newPos.z += distanceBetweenSpawns;
            this.transform.position = newPos;   // Update spawner position

            if (curSpawnIter == maxSpawnIter || newPos.z - this.playerTransform.position.z > this.initialDistanceFromPlayer)
            {
                this.stopSpawning = true;
            }
            this.OnObstaclesSpawned();
        }
    }

    private float getGroundLeftBoundary()
    {
        return this.groundTransform.position.x - this.groundTransform.localScale.x / 2 + this.transform.localScale.x / 2;
    }

    private float getGroundRightBoundary()
    {
        return this.groundTransform.position.x + this.groundTransform.localScale.x / 2 - this.transform.localScale.x / 2;
    }

    private bool allowSpawn(float val)      // Check if the value allows an obstacle to be spawned there
    {
        return val <= this.spawnChance;
    }

    // Protected methods
    protected virtual void OnObstaclesSpawned() { }
    protected virtual void OnBeforeObstaclesSpawned() { }

    // Abstract methods
    abstract public List<List<float>> generateObstacles();
}
