using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool gameHasEnded = false, gamePaused = false;
    public static float currentDistance = 0;
    public static float highScore = 0;

    public float restartDelay = 0.5f;

    public GameObject completeLevelUI, pauseLevelUI, activeScoreUI, gameOverUI, highScoreUI;

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (GameManager.gamePaused) this.Resume();
            else this.Pause();
        }
    }

    public void CompleteLevel ()
    {
        this.completeLevelUI.SetActive(true);
    }

    public void EndGame ()
    {
        if (GameManager.gameHasEnded == false)
        {
            GameManager.gameHasEnded = true;
            Debug.Log("GAME OVER");
            Invoke("ShowGameOverScreen", this.restartDelay);
            //Invoke("Restart", this.restartDelay);
        }
    }
    public void ChangeVolume(float newValue)
    {
        AudioListener.volume = newValue;
    }

    public void Pause()
    {
        GameManager.gamePaused = true;
        Time.timeScale = 0;
        this.pauseLevelUI.SetActive(true);
        this.activeScoreUI.SetActive(false);
    }

    public void Resume()
    {
        GameManager.gamePaused = false;
        Time.timeScale = 1;
        this.pauseLevelUI.SetActive(false);
        this.activeScoreUI.SetActive(true);
    }

    public void Restart()
    {
        GameManager.gameHasEnded = false;
        GameManager.currentDistance = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void ShowGameOverScreen()
    {
        highScoreUI.SetActive(false);
        activeScoreUI.SetActive(false);
        gameOverUI.SetActive(true);
    }
}
