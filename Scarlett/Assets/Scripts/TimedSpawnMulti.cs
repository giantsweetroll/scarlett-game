using System.Collections.Generic;
using UnityEngine;

public class TimedSpawnMulti : MonoBehaviour
{
    public List<GameObject> spawnees = new List<GameObject>();
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;
    public int spawnItemIndex = -1;     // Index of list to spawn, set to -1 to randomize
    public bool randomizeSpawnOffset = false;

    // Start is called before the first frame update
    void Start()
    {
        if (spawnItemIndex < -1) spawnItemIndex = -1;
        else if (spawnItemIndex >= this.spawnees.Count) spawnItemIndex = this.spawnees.Count - 1;

        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    public void SpawnObject()
    {
        Vector3 spawnedPos = this.transform.position;
        
        if (this.randomizeSpawnOffset)
        {
            // Randomized spawn offset
            int change;     // Chance whether to change x and z positions
            // Randomize x pos
            change = Random.Range(1, 11);
            if (change >= 0)
            {
                double offset = Random.Range(-1 * this.transform.localScale.x / 2, this.transform.localScale.x / 2);      // With max and min range
                spawnedPos.x = spawnedPos.x + (float)offset;
            }
            // Randomize z pos
            change = Random.Range(1, 11);
            if (change >= 0)
            {
                double offset = Random.Range(-1 * this.transform.localScale.z / 2, this.transform.localScale.z / 2);      // With max and min range
                spawnedPos.z = spawnedPos.z + (float)offset;
            }
        }

        if (spawnItemIndex == -1)
        {
            int index = Random.Range(0, this.spawnees.Count);
            if (index == this.spawnees.Count) index--;
            Instantiate(this.spawnees[index], spawnedPos, transform.rotation);
        }
        else Instantiate(this.spawnees[this.spawnItemIndex], spawnedPos, transform.rotation);

        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
}
