using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    public void Quit ()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void MainMenu ()
    {
        GameManager.gameHasEnded = true;
        GameManager.currentDistance = 0;
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
