using UnityEngine;

public class ObstacleCollision : MonoBehaviour
{
    public Transform playerTransform;

    private AudioSource collisionAudio;

    public void Awake()
    {
        this.collisionAudio = this.gameObject.GetComponent<AudioSource>();
    }

    public void Start()
    {
        this.collisionAudio.enabled = false;
    }

    private void Update()
    {
        if (this.transform.position.z - this.playerTransform.position.z < 10)
        {
            this.collisionAudio.enabled = true;
        }
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (this.collisionAudio.enabled)
        {
            AudioSource.PlayClipAtPoint(this.collisionAudio.clip, this.transform.position); // Play collision audio
        }
    }
}
