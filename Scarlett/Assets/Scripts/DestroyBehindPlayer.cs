using UnityEngine;

public class DestroyBehindPlayer : MonoBehaviour
{
    public Transform player;
    public float minDistanceBeforeDestroy = 20;

    // Update is called once per frame
    void Update()
    {
        print("Player position: " + this.player.transform.position);
        float distance = this.player.position.z - this.transform.position.z;
        
        if (this.transform.position.y < -1f || distance > minDistanceBeforeDestroy)
        {
            Destroy(this.gameObject);
            print("Object Destroyed");
        }
    }
}
