using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y < -1f) Destroy(this.gameObject);
    }
}
