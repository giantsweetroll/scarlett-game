using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    public float time = 10;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 10);
    }
}
